extends Node

var food_available := false

func _ready():
	$food.visible = false
	$Area.set_deferred("monitoring", false)
	

func give_food():
	food_available = true
	$food.visible = true
	$Area.set_deferred("monitoring", true)
	$AudioStreamPlayer3D.play()

	$"%ComputerUI".food_ready()


func _on_Area_body_entered(body):
	if body.name == "Player" and food_available:
		food_available = false
		$food.visible = false
		$Area.set_deferred("monitoring", false)
		$"%Player".change_hunger(.5)
		if not $AudioStreamPlayer.playing:
			$AudioStreamPlayer.play()
