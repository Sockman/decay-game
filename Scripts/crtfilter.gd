extends Spatial


# Declare member variables here. Examples:
# var a = 2
export var time = 0
export var resolution = Vector3(550,450,0)
export var screenSize = Vector3(1100.0,900,0.0)
export var frameRate = 30
export var viewportTexture: ViewportTexture
export var threshMax: float = 0.8
export var threshMin: float = 0.5 
var screen;
# Called when the node enters the scene tree for the first time.
func _ready():
	screen = $"Cube".get_surface_material(1)
	screen.set_shader_param("resolution",resolution)
	screen.set_shader_param("t1",viewportTexture)
	screen.set_shader_param("frameRate",frameRate)
	screen.set_shader_param("screenSize",screenSize)
	screen.set_shader_param("threshMin",threshMin)
	screen.set_shader_param("threshMax",threshMax)
# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	time+=delta
	screen.set_shader_param("time",time)
	screen.set_shader_param("timeDelta",delta)	
