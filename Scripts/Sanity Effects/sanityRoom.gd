extends Spatial


# Declare member variables here. Examples:
# var a = 2
# var b = "text"
var player : Node;
var material;
var currentPlayerSanity = 1.0;
var prevSanity = 1.0;
var time = 0.0;
var startT=1.0;
var endT=1.0;

# Called when the node enters the scene tree for the first time.
func _ready():
	material = get_node("Cube").get_active_material(0);
	player = get_parent().get_node("Player");

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	time += delta;
	
	var pInsInt : int = round(player.insanity*1000);
	var currentPlayerSanityInt : int = round(currentPlayerSanity*1000);
	if (pInsInt != 1000-currentPlayerSanityInt):
		#print_debug("insanity in room",pInsInt)
		#print_debug("current insanity in room",1000-currentPlayerSanityInt)
		endT = 1.0-player.insanity
		startT = currentPlayerSanity;
		if(time < (1.0/material.get_shader_param("speed"))):
			startT = lerp(prevSanity,currentPlayerSanity,time*material.get_shader_param("speed"))
		time = 0.0
		prevSanity = currentPlayerSanity
		#print_debug(material.get_shader_param("speed"))
		#print_debug(1.0-player.insanity)
		currentPlayerSanity = float(1000-pInsInt)/1000.0
		#print_debug("sanity",currentPlayerSanity)
	#if(time<1.0):
		#print_debug("time:",time)
		#print_debug("startT:",startT)
		#print_debug("endT:",endT)
	material.set_shader_param("time",time)
	#print_debug("time",time)
	material.set_shader_param("startT",startT)
	material.set_shader_param("endT",endT);
#	pass
