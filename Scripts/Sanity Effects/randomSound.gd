extends AudioStreamPlayer3D


export var bias : Curve

# Called when the node enters the scene tree for the first time.
func _ready():
	_update()

func _update():
	while true:
		if bias.interpolate(randf()) < $"%Player".insanity and not is_playing():
			play()
		
		# wait from 5-10 seconds
		yield(get_tree().create_timer((randi() % 5) + 5, false), "timeout")
