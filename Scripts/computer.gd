extends Node


const creepy_text = [
	"you are alone.",
	"don't turn around.",
	"murderer."]
 
var text_display_timer := 0.0
var history = []
var index = 0
var tabCompletionInProgress = false
var compResults = []
var resultsIndex = -1
var finishBool = false
var compResultsLength = 0
const text_display_interval := .01

var last_game_state

func _process(delta):
	text_display_timer += delta
	if text_display_interval < text_display_timer:
		text_display_timer = 0
		if $RichTextLabel.visible_characters < $RichTextLabel.get_total_character_count():
			$RichTextLabel.visible_characters += 1
		
	$LineEdit.focus_mode = Control.FOCUS_ALL
	if($LineEdit.text.length()>0 and $LineEdit.text[$LineEdit.text.length()-1]==' '):
		tabCompletionInProgress = false
		compResults = []
		resultsIndex = -1
		compResultsLength =0
	if($LineEdit.text.length()!=compResultsLength):
		tabCompletionInProgress = false
		compResults = []
		resultsIndex = -1
		compResultsLength =0

	

	if last_game_state != CountryManager.game_state:
		match(CountryManager.game_state):
			CountryManager.GameState.notesent:
				write_text("ATTENTION")
				write_text("	WE HAVE PLANTED A BUG WITHIN THIS SYSTEM. THEY CANNOT SEE US.")
				write_text("	YOU HAVE BEEN NOTICED. WE BELIEVE YOU CAN BE OF USE.")
				write_text("	SOON YOU WILL BE GRANTED POWER. UNTIL IT ACTIVATES, CONTINUE AS YOU HAVE BEEN.")
			CountryManager.GameState.endgame:
				write_text("ATTENTION")
				write_text("	YOU CAN NOW ATTACK YOUR OWN COUNTRY")
			CountryManager.GameState.alldead:
				write_text("The population of our enemies has been decimated.")
				write_text("Your %d days of nonstop hard work shall be rewarded soon."% [$"../../Player".day])
				write_text("Although some have speculated you could have it more efficiently.")
				finishBool = true
		last_game_state = CountryManager.game_state

func food_ready():
	write_text("Your food has been delivered. It's your favorite.")

func _ready():
	_handle_random_events()
	history.append("")
	last_game_state = CountryManager.game_state

var event_timer_message := 0.0
var event_timer_glitchedtext := 0.0
func _handle_random_events():
	while true:
		event_timer_message -= 0.1
		event_timer_glitchedtext -= 0.1
		yield(get_tree().create_timer(0.1, false), "timeout")
		
		# Random glitched text
		if event_timer_glitchedtext <= 0 and $"%Player".insanity > .5 and randf() > 0.2:
			if $RichTextLabel.text.length() == 0: continue
			
			var rand_char := char(randi() % 94 + 32)
			var position : int = 0
			if $RichTextLabel.text.length() > 200:
				position = $RichTextLabel.text.length() - (randi() % 200 + 1)
			else:
				position = randi() % $RichTextLabel.text.length()
			if 	$RichTextLabel.text[position] == "\n": continue
			
			var prev_char : String = $RichTextLabel.text[position]
			$RichTextLabel.text[position] = rand_char 
			_return_char_to_normal(position, prev_char)
			event_timer_glitchedtext += randf() * (30 - ($"%Player".insanity * 29.5))
			print($"%Player".insanity)
		
		# Random creepy message
		if event_timer_message <= 0 and $"%Player".insanity > .7 and randf() > 0.1:
			var text_to_write : String = creepy_text[randi() % creepy_text.size()]
			var length : int = $RichTextLabel.text.length()
			write_text(text_to_write, false)
			yield(get_tree().create_timer(0.8, false), "timeout")
			$RichTextLabel.text = $RichTextLabel.text.substr(0, length) + $RichTextLabel.text.substr(length + text_to_write.length())
			event_timer_message += randf() * 60 + 15

func _return_char_to_normal(position : int, prev_char : String):
	yield(get_tree().create_timer(randf(), false), "timeout")
	$RichTextLabel.text[position] = prev_char 

var first_select := true
func select():
	if first_select:
		first_select = false
		write_text("Welcome to your new home. Your government has taken note of your skills, and rather than dismiss them - we have been been kind enough to grant you a great task.")
		write_text("Our military might is great, but its use has stagnated  in recent years. We must have war, and once again our military will be the greatest power in the world.")
		write_text("This is your great task. With the terminal in front of you, you will plant the seeds of war. Your reward will be the privilege of living.")
		write_text("Good luck, and glory to the supreme leader!")
	$LineEdit.grab_focus()


func deselect():
	$LineEdit.release_focus()
	pass


func write_text(newtext : String, newline : bool = true):
	var current_count = $RichTextLabel.visible_characters
	$RichTextLabel.append_bbcode(newtext + ( "\n" if newline else ""))
	$RichTextLabel.visible_characters = current_count


func _input(event):
	if event.is_action_pressed("ui_accept"):
		if $RichTextLabel.visible_characters < $RichTextLabel.get_total_character_count():
			$RichTextLabel.visible_characters = $RichTextLabel.get_total_character_count()
			return
		var newtext = $LineEdit.text
		write_text(newtext)
		_process_command(newtext)
		index = history.size()
		$LineEdit.text = ""
	if event.is_action_pressed("ui_hist_up"):
		index = max(0,index-1)
		$LineEdit.text = history[index]
		yield(get_tree(), "idle_frame")
		$LineEdit.set_cursor_position($LineEdit.text.length())
	if event.is_action_pressed("ui_hist_down"):
		index = min(history.size()-1,index+1)
		$LineEdit.text = history[index]
		$LineEdit.set_cursor_position($LineEdit.text.length())
	if event.is_action_pressed("ui_tab_comp"):
		if(tabCompletionInProgress):
			$LineEdit.text = started_tab_comp()
			$LineEdit.set_cursor_position($LineEdit.text.length())
		else:
			$LineEdit.text = tab_comp($LineEdit.text);
			$LineEdit.set_cursor_position($LineEdit.text.length())
	if event.is_action_pressed("ui_clear"):
		$LineEdit.text = ""
		tabCompletionInProgress = false
		compResults = []
		resultsIndex = -1	
		compResultsLength = 0

func started_tab_comp():
			resultsIndex=(resultsIndex+1)%compResults.size();
			var com = $LineEdit.text.split(" ")
			var newString = ""
			for i in range(com.size()-1):
				newString += com[i]+" "
			newString += compResults[resultsIndex]
			compResultsLength = newString.length()
			return newString

func tab_comp(command: String):
	var currentCommand = command.split(" ")
	var lastIndex = currentCommand.size()-1
	var commandString = ""
	var twoWordCommands =["hack","misinform","leak"]
	var threeWordCommands = ["misinform","leak"]
	if(lastIndex == -1 or (lastIndex == 0 and (command.length() == 0 or(command.length() > 0 and command[command.length()-1]!= ' ')))):
		commandString = "help list hack misinform leak"
	elif(((lastIndex == 1 or lastIndex==0) and twoWordCommands.has(currentCommand[0])) or (lastIndex == 2 and (threeWordCommands.has(currentCommand[0])))):
		commandString = "Oceana Pacifica Atlantica Fedara"
	else:
		return command
	var regex = RegEx.new()
	var midString = ""
	if((lastIndex == 0 and command.length()==0) or (command.length()>0 and command[command.length()-1] == ' ')):
		midString = "\\w)"
	else:
		midString = currentCommand[lastIndex]+")\\S*"
	regex.compile("\\S*("+midString+"\\S *")
	var results = []
	for res in regex.search_all(commandString):
		results.push_back(res.get_string())
	
	var result = ""
	if(results.size()>0):
		for i in range(results.size()):
			if(results[i][results[i].length()-1]==' '):
				compResults.append(results[i].substr(0,results[i].length()-1))
			else:
				compResults.append(results[i])
		result = compResults[0]
		tabCompletionInProgress = true
		resultsIndex = 0	
	else:
		return command
	
	var newString = ""
	for i in  range(lastIndex):
		newString += currentCommand[i] + " "
	newString += result
	compResultsLength = newString.length()
	return newString
	
func _process_command(command : String):
	var split = command.split(" ")
	match(split[0]):
		"help":
			write_text("Commands:")
			write_text("	help")
			write_text("		displays all commands and their function")
			write_text("	list")
			write_text("		displays a list of available locations to target")
			write_text("")
			write_text("	These commands issue a payout of Base + Bonus - Penalty:")
			write_text("	hack <country>")
			write_text("		do a (mostly) harmless attack on government system")
			write_text("		(Base pay: %d)" %[CountryManager.Commands.hack.pay])
			write_text("	misinform <country>")
			write_text("		spread misinformation within a country")
			write_text("		(Base pay: %d)" %[CountryManager.Commands.misinform.pay])
			write_text("	leak <country1> <country2>")
			write_text("		leak government secrets from country1 to country2")
			write_text("		(Base pay: %d)" %[CountryManager.Commands.leak.pay])
		"list":
			write_text("Available Countries")
			for country in CountryManager.Countries.keys():
				write_text("	" + country)
		"hack":
			_hack(split)
			pass
		"misinform":
			_misinform(split)
			pass
		"leak":
			_leak(split)
			pass
		_:
			write_text("Unrecognized command. Type \"help\" for help.")
	history.append(command)
	write_text("")


func _hack(params : Array):
	if params.size() < 2:
		write_text("Usage: hack <country>")
		return
	if CountryManager.Countries.has(params[1]):
		write_text(CountryManager.aggro_hack(params[1]))
	else:
		write_text("Unrecognized country. Use \"list\".")


func _misinform(params : Array):
	if params.size() < 2:
		write_text("Usage: misinform <country>")
		return
	if CountryManager.Countries.has(params[1]):
		write_text(CountryManager.aggro_misinform(params[1]))
	else:
		write_text("Unrecognized country. Use \"list\".")
	
func _leak(params : Array):
	if params.size() < 3:
		write_text("Usage: leak <country> <country>")
		return
	if CountryManager.Countries.has(params[1]) and CountryManager.Countries.has(params[2]):
		write_text(CountryManager.aggro_leak(params[1], params[2]))
	else:
		write_text("Unrecognized country. Use \"list\".")
