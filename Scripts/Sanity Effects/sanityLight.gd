extends SpotLight


# Declare member variables here. Examples:
# var a = 2
# var b = "text"
export var startColor : Color
export var peakColor : Color
export var startEnergy : float
export var peakEnergy : float
export var emmissionColor : Color

var currentWeight:float
var prevInsanity:float
var time
export var speed: float


# Called when the node enters the scene tree for the first time.
func _ready():
	currentWeight = $'%Player'.insanity
	prevInsanity = $'%Player'.insanity
	pass # Replace with function body.


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	
	var lowW= 0
	var highW = 1
	var insanity = $"%Player".insanity
	
	if(currentWeight<= insanity):
		lowW = currentWeight 
		highW = insanity
	else:
		lowW = insanity 
		highW = currentWeight
	get_parent().get_node("Cylinder").mesh.surface_get_material(1).emission = startColor.linear_interpolate(emmissionColor,currentWeight)
	currentWeight = max(min(lerp(currentWeight,insanity,delta*speed),highW),lowW)
	light_color = startColor.linear_interpolate(peakColor,currentWeight)
	light_energy = lerp(startEnergy,peakEnergy,currentWeight)
