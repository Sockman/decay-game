extends Node


# Declare member variables here. Examples:
# var a = 2
# var b = "text"

var enable := false

func endgame():
	$AudioStreamPlayer3D.play()
	$Area.set_deferred("monitoring", true)
	enable = true


func _on_Area_body_entered(body):
	if body.name == "Player" and enable:
		get_tree().quit()
