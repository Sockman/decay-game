extends KinematicBody

const GRAVITY := 5

const scoreThresholdInterval := 5
const scoreThresholdMax := 200
var scoreThreshold := 100

# Speed values (meters/second)
export var speed : float

export var computerUI : NodePath

export var insanityAmount = 0.05

var day : int = 0
var score : int = 0
var hunger : float = 1.0
var insanity : float = 0.0
var time : float = 1.0
var originPosition : Vector3

var healEnter = false;

enum States{
	normal,
	screen,
}
var state = States.normal

var crazyBool = false
var game_over := false

func pass_day(numberOfDays :int) -> void:
	day += numberOfDays;

func add_score(amount : int) -> void:
	score += amount
	
	if score > scoreThreshold:
		score -= scoreThreshold
		get_parent().get_node("%Food").give_food()
		scoreThreshold = int(min(scoreThresholdInterval + scoreThreshold, scoreThresholdMax))

func change_insanity(amount : float) -> void:
	insanity += amount
	insanity = clamp(insanity, 0, 1)
	if(!(insanity<1.0)):
		get_node("TextureRect").toggle_fade_shader();
		crazyBool = true
		state = States.normal
		time = 0.0

func change_hunger(amount : float) -> void:
	hunger += amount
	hunger = clamp(hunger, 0, 1)

func rotateVec(var v: Vector3,var q : Quat):
	var u:Vector3 = Vector3(q.x,q.y,q.z)
	var s:float = q.w
	var vprime = (2.0 * u.dot(v) * u) +(s*s-u.dot(u))*v + (2.0*s*u.cross(v)) 
	return vprime

func _physics_process(_delta : float) -> void:
	if game_over:
		return
	
	$HungerBar.value = hunger
	hunger -= _delta * .004
	
	if hunger <= 0:
		game_over = true
		Input.set_mouse_mode(Input.MOUSE_MODE_VISIBLE)
		state = States.normal
		$TextureRect.toggle_fade_shader()
		var tween = get_tree().create_tween()
		$Label.modulate = Color(1,1,1,0)
		tween.tween_property($Label, "modulate", Color(1,1,1,1), 3)
		if day == 1:
			$Label.text = "You went hungry after %d day." % [day]
		else:
			$Label.text = "You went hungry after %d days." % [day]
		$Button.disabled = false
		tween.tween_property($Button, "modulate", Color(1,1,1,1), 3)
		
	
	if state == States.screen:
		return
	
	var movement := Vector3.ZERO
	#get inputs
	var dir := Vector3.ZERO
	if Input.is_action_pressed("move_forward"):
		dir += Vector3(0,0,-1)
	if Input.is_action_pressed("move_backward"):
		dir += Vector3(0,0,1)
	if Input.is_action_pressed("move_left"):
		dir += Vector3(-1,0,0)
	if Input.is_action_pressed("move_right"):
		dir += Vector3(1,0,0)
		
	dir = dir.normalized()
	
		
	#offset by camera rotation
	var cam_rot : float = $Camera.rotation.y
	dir = dir.rotated(Vector3.UP, cam_rot)
	
	movement = dir * speed
	
	#apply gravity
	if not is_on_floor():
		movement -= Vector3(0,GRAVITY,0)
		
	#apply all movement
	move_and_slide(movement, Vector3.UP, false)
	
	
func _input(event : InputEvent) -> void:

	if event.is_action_pressed("interact"):
		match(state):
			States.normal:
				$"%RayCast".force_raycast_update()
				if $"%RayCast".get_collider() != null and $"%RayCast".get_collider().name == "ComputerCollider":
					state = States.screen
					get_node(computerUI).select()
					
					if $Instructions.modulate.a != 0:
						var tween = get_tree().create_tween()
						tween.tween_property($Instructions, "modulate", Color(1,1,1,0), 2)
					
			States.screen:
				state = States.normal
				get_node(computerUI).deselect()
	
	if (state == States.screen):
		get_node(computerUI).get_parent().input(event)

func _ready():
	originPosition = translation
	var tween = get_tree().create_tween()
	$Instructions.modulate = Color(1,1,1,0)
	tween.tween_property($Instructions, "modulate", Color(1,1,1,1), 2)
	$Camera.cam_focus_loc = $"../CameraFocus".transform
func _process(delta):
	time+=delta
	if(time > 1.0):
		if(Input.is_action_pressed("addInsanity")):
			time = 0;
			change_insanity(.05)
	if(healEnter):
		var rotX = get_node("Camera").rot_x
		var rotY = get_node("Camera").rot_y
		var quat1 = Quat(Vector3(rotX, rotY, 0))
		var origin = rotateVec(Vector3.FORWARD,quat1);
		if(origin.angle_to(Vector3.UP)<PI/12):
			insanity = max(0.0,insanity-insanityAmount*delta)	
		else:
			insanity = max(0.0,insanity-0.01*delta)
		
	if(crazyBool):
		var fadeTime = (1.0/$TextureRect.faderSpeed +1.0)
		if (time > fadeTime):
			$"../Viewport/ComputerUI".deselect()
			translation = originPosition
			$TextureRect.toggle_fade_shader()
			crazyBool = false
			pass_day(30)
			insanity = 0.0

func _on_Area_body_entered(_body):
	healEnter=true

func _on_Area_body_exited(_body):
	healEnter=false
	
	pass # Replace with function body.
