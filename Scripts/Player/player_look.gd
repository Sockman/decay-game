extends Camera

const head_height := 0.8

export var look_speed : float
export var mouse_smoothing : float
export var anim_speed : float

export var cam_focus_loc : Transform

var rot_x := 0.0
var rot_y := 0.0
var rot_y_screen := 0.0

var is_paused := true

enum States{
	normal,
	screen,
}

# Called when the node enters the scene tree for the first time.
func _ready() -> void:
	Input.set_mouse_mode(Input.MOUSE_MODE_CAPTURED)


func _physics_process(_delta : float) -> void:
	if $"..".game_over:
		return
	
	match($"..".state):
		States.normal:
			var new_translation = Vector3(0, head_height, 0) + get_parent().translation
			global_transform.origin = lerp(global_transform.origin, new_translation, anim_speed)

			var quat1 := Quat(Vector3(rot_x, rot_y, 0))
			global_transform.basis = global_transform.basis.slerp(quat1, mouse_smoothing)
		States.screen:
			global_transform.origin = lerp(global_transform.origin, cam_focus_loc.origin, anim_speed)
			#global_transform.basis = Basis(global_transform.basis.slerp(cam_focus_loc.basis, anim_speed))
			
			var quat1 := Quat(Vector3(cam_focus_loc.basis.get_euler().x, cam_focus_loc.basis.get_euler().y + rot_y_screen, 0))
			global_transform.basis = global_transform.basis.slerp(quat1, mouse_smoothing)


func _input(event : InputEvent) -> void:
	if event is InputEventMouseMotion:
		
		if $"..".state == States.normal:
			rot_x -= event.relative.y * look_speed
			rot_y -= event.relative.x * look_speed
			rot_y = fmod(rot_y, 2 * PI)
			rot_x = clamp(rot_x, -PI/2, PI/2)
		else:
			rot_y_screen -= event.relative.x * look_speed
			rot_y_screen = clamp(rot_y_screen, 0, PI/4.0)


