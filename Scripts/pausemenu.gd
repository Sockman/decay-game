extends Control

var is_paused := false

func _ready():
	toggle_pause(false)

func _input(event : InputEvent) -> void:
	if event.is_action_pressed("escape"):
		toggle_pause(not is_paused);

func toggle_pause(state) -> void:
	Input.set_mouse_mode(Input.MOUSE_MODE_VISIBLE if state else Input.MOUSE_MODE_CAPTURED)
	get_tree().paused = state
	visible = state
	is_paused = state
