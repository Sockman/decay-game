extends Node

enum GameState {
	normal,
	notesent,	# Note is sent
	endgame,	# Every country is destroyed but your home country
	alldead		# Everyone is dead
}

var game_state = GameState.normal

var prevMove : int = -1
var repeatPunish := 0
var fibMax := 21
var total_pop := 5000
var relationship_matrix = []
var prevRP := 0


class CountryClass:
	const bonusMax := 5
	
	var name : String
	var index : int
	var population : int
	var leader_title : String
	var destroyed : bool
	var birthrate : int
	
	var bonus : int
	
	func _init( name_p : String, index_p : int, population_p : int, leader_title_p : String):
		self.name = name_p
		self.index = index_p
		self.population = population_p
		self.leader_title = leader_title_p
		self.bonus = randi()%bonusMax
		self.destroyed = false
		self.birthrate = 100
		
	func pass_turn():
		if self.destroyed:
			self.bonus = 0
		else:
			self.bonus = randi()%bonusMax
			
	func set_birthrate(totalRel):
		self.birthrate =  int(100 - pow(totalRel, 2.0))

	func destroy(relationship_matrix : Array):
		self.destroyed = true
		self.bonus = 0
		for i in range(0, relationship_matrix.size()):
			relationship_matrix[index][i] = 0
			relationship_matrix[i][index] = 0

var Countries = {
	"Pacifica" : CountryClass.new("Pacifica", 0, 3502192, "President"),
	"Oceana" : CountryClass.new("Oceana", 1, 2992235, "Prime Minister"),
	"Atlantica" : CountryClass.new("Atlantica", 2, 4525921, "Head Chairman"),
	"Fedara" : CountryClass.new("Fedara", 3, 5849284, "Supreme Leader")
}

var homeCountry := "Fedara";

class CommandClass:
	var pay : int
	var cost : int
	var relVal : int
	
	var bonus : int
	var bonusMax : int
	
	func _init(var pay_p : int, var cost_p : int, var relVal_p : int, var bonusMax_p):
		self.pay = pay_p
		self.cost = cost_p
		self.relVal = relVal_p
		self.bonusMax = bonusMax_p
	
	func pass_turn():
		self.bonus = randi()%bonusMax

var Commands = {
	"hack" : CommandClass.new(10, 5, 5, 5),
	"misinform" : CommandClass.new(30, 3, 10, 5),
	"leak" : CommandClass.new(20, 7, 7, 5),
}

# Called when the node enters the scene tree for the first time.
func _ready():
	if get_tree().current_scene.get_class() == "Control":
		return
	else:
		start()


var game_updating := false
# Call this when the game begins or restarts to reset all variables
func start():
	total_pop = 5000
	for x in range(Countries.size()):
		relationship_matrix.append([])
		relationship_matrix[x]=[]        
		for y in range(Countries.size()):
			relationship_matrix[x].append([])
			relationship_matrix[x][y] = 0

func _process(_delta):
	if not game_updating and not relationship_matrix.empty():
		_update()
	
func _update():
	if game_updating:
		return
	game_updating = true
	
	while true:
		var total_pop_tmp := 0
		
		for country in Countries.values():
			if country.destroyed:
				continue
			
			var totalRel := 0
			for country2 in Countries.values():
				totalRel += relationship_matrix[country.index][country2.index]
				totalRel += relationship_matrix[country2.index][country.index]
			
			# Set birthrate
			country.set_birthrate(totalRel)
			
			# Change population
			if total_pop < 1000:
				country.population += (randi() % 100) + country.birthrate
			else:
				country.population += (randi() % 100 - 50) + country.birthrate
		
			# Detect if population reaches 0
			if country.population <= 0 or country.destroyed:
				country.population = 0
				country.destroy(relationship_matrix)
			
			# Prevent home country from being destroyed before the others
			if Countries[homeCountry].population < country.population:
				Countries[homeCountry].population += (country.population - Countries[homeCountry].population) + (randi() % 100)
			
			# Get total population
			total_pop_tmp += country.population
		
		total_pop = total_pop_tmp
		
		if total_pop == 0:
			game_state = GameState.alldead
		elif total_pop <= Countries[homeCountry].population:
			game_state = GameState.endgame
		elif total_pop - Countries[homeCountry].population < 9000000:
			game_state = GameState.notesent
		
		if get_node_or_null("../Spatial/Player") != null:
			get_node("../Spatial/Player").pass_day(1)
		
		update_status()
		
		yield(get_tree().create_timer(5, false), "timeout")


func fibRatio():
	return 1.0-(min(repeatPunish,fibMax)/fibMax)


# country1 is hacked (gets aggrod to another random country)
func aggro_hack(country1 : String):
	if Countries[country1].destroyed:
		return "There is nothing to hack in %s." % country1
		
	if (game_state == GameState.normal or game_state == GameState.notesent) and country1 == homeCountry:
		return "Do not attack your home."
	
	check_repeat(0)
	relationship_matrix[Countries[country1].index][randi() % Countries.size()] += round(Commands.hack.relVal * fibRatio())
	get_node("../Spatial/Player").change_insanity(.05)
	var pay = max(0,Commands.hack.pay + Countries[country1].bonus + Commands.hack.bonus - repeatPunish)
	
	var return_msg = ""
	if pay == 0:
		return_msg = "This method has become inefficient. You were paid %d foodbux." % [pay]
	else:
		return_msg = "Good work. You were paid %d (Base) + %d + %d Bonus) - %d (Penalty) = %d foodbux." % [Commands.hack.pay, Countries[country1].bonus, Commands.hack.bonus, repeatPunish, pay]
	
	get_node("../Spatial/Player").add_score(pay)
	get_node("../Spatial/Player").pass_day(Commands.hack.cost)
	
	pass_turn()
	return return_msg


# country1 is misinformed
func aggro_misinform(country1 : String):
	if Countries[country1].destroyed:
		return "There is nobody in %s to misinform." % country1
	
	if (game_state == GameState.normal or game_state == GameState.notesent) and country1 == homeCountry:
		return "Do not attack your home."
	
	relationship_matrix[Countries[country1].index][Countries[country1].index] += round(Commands.misinform.relVal * fibRatio())
	get_node("../Spatial/Player").change_insanity(.1)
	var pay = max(0, Commands.misinform.pay + Countries[country1].bonus + Commands.misinform.bonus - repeatPunish)
	
	var return_msg = ""
	if pay == 0:
		return_msg = "This method has become inefficient. You were paid %d foodbux." % [pay]
	else:
		return_msg = "Excellent job. You were paid %d (Base) + %d + %d (Bonus) - %d (Penalty) = %d foodbux." % [Commands.misinform.pay, Countries[country1].bonus, Commands.misinform.bonus, repeatPunish, pay]
	
	get_node("../Spatial/Player").add_score(pay)
	get_node("../Spatial/Player").pass_day(Commands.misinform.cost)
	check_repeat(1)
	pass_turn()
	return return_msg


# country1 info is leaked to country2, so country2 hates country1
func aggro_leak(country1 : String, country2 : String):
	if Countries[country1].destroyed:
		return "There is nobody in %s to leak information from." % country1
	if Countries[country2].destroyed:
		return "There is nobody in %s to leak information to." % country2
	
	if (game_state == GameState.normal or game_state == GameState.notesent) and country1 == homeCountry:
		return "Do not leak information from your home."
	
	relationship_matrix[Countries[country1].index][Countries[country2].index] += round(Commands.leak.relVal * fibRatio())
	get_node("../Spatial/Player").change_insanity(.075)
	var pay = max(0, Commands.leak.pay + Countries[country1].bonus + Commands.leak.bonus - repeatPunish)
	
	var return_msg = ""
	if pay == 0:
		return_msg = "This method has become inefficient. You were paid %d foodbux." % [pay]
	else:
		return_msg =  "Flawless execution. You were paid %d (Base) + %d + %d (Bonus) - %d (Penalty) = %d foodbux." % [Commands.leak.pay, Countries[country1].bonus, Commands.leak.bonus, repeatPunish, pay]
	
	get_node("../Spatial/Player").add_score(pay)
	get_node("../Spatial/Player").pass_day(Commands.leak.cost)
	check_repeat(2)
	pass_turn()
	return return_msg
		

func check_repeat(type:int):
	if(prevMove==type):
		if(repeatPunish == 0):
			repeatPunish+=1
		else:
			var tempRP = repeatPunish
			repeatPunish = int(min(fibMax, repeatPunish + prevRP))
			prevRP = tempRP
	else:
		repeatPunish=0
		prevRP =0
	prevMove = type


func pass_turn():
	for country in Countries.values():
		country.pass_turn()
	for command in Commands.values():
		command.pass_turn()
	update_status()


const peace_msgs = [
	"	The world is at relative peace\n",
]

const civil_msgs = [
	"	There is civil discontent within {0}.\n",
	"	Mass protests have broken out in {0}.\n",
	"	Police arm against riots in {0}.\n",
	"	Riots continue across {0}. The {1} affirms the situation is under control.\n",
	"	Officials flee {1} amid concerns of safety.\n",
	"	The {1} of {0} has been killed by a coup. The country's future is unknown.\n",
]

const relation_msgs = [
	"	{0} is upset with {1}.\n",
	"	{0} believes {1} is responsible for sabotage, threatens action.\n",
	"	Bombs kill 50 in {1}. Authorities believe {0} is responsible.\n",
	"	{1} sabotages miliary supplies headed for {0}.\n",
	"	{0} has declared war on {1}.\n",
	"	{1} refuses a peace treaty as war continues with {0}.\n",
]

const destroyed_msg = "	{0} is destroyed.\n"

var status : String

# Get status of countries
func update_status() -> void:
	var new_status := ""
	
	new_status += "News:\n"

	# Add news
	var temp_matrix = str2var(var2str(relationship_matrix))
	for x in Countries.values():
		
		if x.destroyed:
			new_status += destroyed_msg.format([x.name])
			continue
		
		for y in Countries.values():
			var rel_value = relationship_matrix[x.index][y.index]
			if rel_value > 10:
				temp_matrix[x.index][y.index] = 0
				var severity = (rel_value)/30
				if x.name == y.name:
					new_status += civil_msgs[min(severity, civil_msgs.size()-1)].format([x.name, Countries[x.name].leader_title])
				else:
					new_status += relation_msgs[min(severity, relation_msgs.size()-1)].format([x.name, y.name])
	print(relationship_matrix)
	
	if new_status.count("\n") == 1:
		new_status += "	The world is at relative peace\n"
	
	new_status += "Population:\n"
	# Populations
	for x in Countries.values():
		new_status += "	The population of %s is %d.\n" % [x.name, x.population]
		new_status += "		Bonus: %d.\n" % x.bonus
	new_status += "	The total population is %d.\n" % [total_pop]
	
	# Command bonuses
	for x in Commands.keys():
		new_status += "%s bonus: %d\n" % [x, Commands[x].bonus]
	
	if game_state == GameState.alldead:
		get_node("../Spatial/EndGame").endgame()
	
	status = new_status
