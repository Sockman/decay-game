extends Node



export (Array, AudioStream) var keypresses

func _on_LineEdit_text_changed(new_text):
	$AudioStreamPlayer.stream = keypresses[randi() % keypresses.size()]
	$AudioStreamPlayer.play()
