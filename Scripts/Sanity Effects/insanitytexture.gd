extends TextureRect


# Declare member variables here. Examples:
# var a = 2
# var b = "text"
var time=0.0
var time2 = 0.0
export var insanityPow = 2;
enum faderShaderStates{
	active,
	inactive
}
var faderSpeed = .5
var fSS = faderShaderStates.inactive
var view : Viewport
# Called when the node enters the scene tree for the first time.
func _ready():
	view = get_viewport()
	stretch_mode = TextureRect.STRETCH_KEEP_ASPECT

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	time+=delta
	#if(Input.is_action_just_pressed("insanity_pow_up")):
	#	insanityPow +=1
	#if(Input.is_action_just_pressed("insanity_pow_down")):
	#	insanityPow = min(0,insanityPow-1)
	rect_scale = (Vector2(view.size.x/1920,view.size.y/1080.0))
	material.set_shader_param("time",time)
	var screenRatio = (1024.0/600.0)/(1920.0/1080.0)
	material.set_shader_param("warpSize",150.0*screenRatio)
	material.set_shader_param("insanity",pow($"%Player".insanity,insanityPow))
	if(fSS == faderShaderStates.active):
		material.set_shader_param("fadeactive",true)
		time2 += delta
		print_debug("time2",time2);
	else:
		material.set_shader_param("fadeactive",false)
	material.set_shader_param("fadetime",time2)
	material.set_shader_param("fadeSpeed",faderSpeed)
	
func toggle_fade_shader():
	if(fSS== faderShaderStates.inactive):
		fSS = faderShaderStates.active
	else:
		fSS = faderShaderStates.inactive
	time2 = 0.0
		
